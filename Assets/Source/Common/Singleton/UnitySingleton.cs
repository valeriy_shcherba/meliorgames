﻿using UnityEngine;

namespace Common.Singleton
{
	public class UnitySingleton<T> : MonoBehaviour where T : UnitySingleton<T>, new()
	{
		private static T _instance;

		public static T Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = (T)FindObjectOfType(typeof(T));

					if (_instance == null)
					{
						GameObject singleton = new GameObject(typeof(T).Name);
						_instance = singleton.AddComponent<T>();
					}
				}

				return _instance;
			}
		}

		void Awake()
		{
			if (_instance == null)
			{
				_instance = this as T;
			}
			else
			{
				Destroy(gameObject);
			}
		}
	}
}