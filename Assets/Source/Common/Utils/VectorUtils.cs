﻿using UnityEngine;

namespace Common.Utils
{
    public class VectorUtils 
    {
        public static Quaternion RotateToObject(Transform target, Transform rotateTo)
        {
            Vector3 difference = target.position - rotateTo.position;
            float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            
            return Quaternion.Euler(0.0f, 0.0f, rotationZ);
        }
    }
}

