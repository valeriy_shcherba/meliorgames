using Common.Signal.Api;
using System;
using System.Linq;

namespace Common.Signal.Impl
{
    public class BaseSignal : IBaseSignal
    {
        private event Action<IBaseSignal, object[]> _baseListener = null;
        private event Action<IBaseSignal, object[]> _onceBaseListener = null;

        public void Dispatch(object[] args)
        {
            if (_baseListener != null)
            {
                _baseListener(this, args);
            }
            if (_onceBaseListener != null)
            {
                _onceBaseListener(this, args);
            }
            _onceBaseListener = null;
        }

        public void AddListener(Action<IBaseSignal, object[]> callback)
        {
            _baseListener = AddUnique(_baseListener, callback);
        }

        public void AddOnce(Action<IBaseSignal, object[]> callback)
        {
            _onceBaseListener = AddUnique(_onceBaseListener, callback);
        }

        public void RemoveListener(Action<IBaseSignal, object[]> callback)
        {
            if (_baseListener != null)
            {
                _baseListener -= callback;
            }
        }

        public virtual void RemoveAllListeners()
        {
            _baseListener = null;
            _onceBaseListener = null;
        }

        private Action<T, U> AddUnique<T, U>(Action<T, U> listeners, Action<T, U> callback)
        {
            if (listeners == null || !listeners.GetInvocationList().Contains(callback))
            {
                listeners += callback;
            }
            return listeners;
        }
    }
}

