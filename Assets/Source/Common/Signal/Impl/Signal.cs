using System;
using System.Linq;

namespace Common.Signal.Impl
{
    public interface ISignal
    {
        Delegate Listener { get; set; }

        void RemoveAllListeners();
    }

    public class Signal : BaseSignal, ISignal
    {
        private event Action _listener = null;
        private event Action _onceListener = null;

        public Delegate Listener
        {
            get { return _listener; }
            set { _listener = (Action)value; }
        }

        public void AddListener(Action callback)
        {
            _listener = AddUnique(_listener, callback);
        }

        public void AddOnce(Action callback)
        {
            _onceListener = AddUnique(_onceListener, callback);
        }

        public void RemoveListener(Action callback)
        {
            if (_listener != null)
            {
                _listener -= callback;
            }
        }

        public void Dispatch()
        {
            if (_listener != null)
            {
                _listener();
            }
            if (_onceListener != null)
            {
                _onceListener();
            }
            _onceListener = null;

            base.Dispatch(null);
        }

        public override void RemoveAllListeners()
        {
            _listener = null;
            _onceListener = null;

            base.RemoveAllListeners();
        }

        private Action AddUnique(Action listeners, Action callback)
        {
            if (listeners == null || !listeners.GetInvocationList().Contains(callback))
            {
                listeners += callback;
            }
            return listeners;
        }
    }

    public class Signal<T> : BaseSignal, ISignal
    {
        private event Action<T> _listener = null;
        private event Action<T> _onceListener = null;

        public Delegate Listener
        {
            get { return _listener; }
            set { _listener = (Action<T>)value; }
        }

        public void AddListener(Action<T> callback)
        {
            _listener = AddUnique(_listener, callback);
        }

        public void AddOnce(Action<T> callback)
        {
            _onceListener = AddUnique(_onceListener, callback);
        }

        public void RemoveListener(Action<T> callback)
        {
            if (_listener != null)
            {
                _listener -= callback;
            }
        }

        public void Dispatch(T type1)
        {
            if (_listener != null)
            {
                _listener(type1);
            }
            if (_onceListener != null)
            {
                _onceListener(type1);
            }
            _onceListener = null;

            object[] outv = { type1 };

            base.Dispatch(outv);
        }

        private Action<T> AddUnique(Action<T> listeners, Action<T> callback)
        {
            if (listeners == null || !listeners.GetInvocationList().Contains(callback))
            {
                listeners += callback;
            }
            return listeners;
        }

        public override void RemoveAllListeners()
        {
            _listener = null;
            _onceListener = null;

            base.RemoveAllListeners();
        }
    }

    public class Signal<T, U> : BaseSignal, ISignal
    {
        private event Action<T, U> _listener = null;
        private event Action<T, U> _onceListener = null;

        public Delegate Listener
        {
            get { return _listener; }
            set { _listener = (Action<T, U>)value; }
        }

        public void AddListener(Action<T, U> callback)
        {
            _listener = AddUnique(_listener, callback);
        }

        public void AddOnce(Action<T, U> callback)
        {
            _onceListener = AddUnique(_onceListener, callback);
        }

        public void RemoveListener(Action<T, U> callback)
        {
            if (_listener != null)
            {
                _listener -= callback;
            }
        }

        public void Dispatch(T type1, U type2)
        {
            if (_listener != null)
            {
                _listener(type1, type2);
            }
            if (_onceListener != null)
            {
                _onceListener(type1, type2);
            }
            _onceListener = null;

            object[] outv = { type1, type2 };

            base.Dispatch(outv);
        }

        public override void RemoveAllListeners()
        {
            _listener = null;
            _onceListener = null;

            base.RemoveAllListeners();
        }

        private Action<T, U> AddUnique(Action<T, U> listeners, Action<T, U> callback)
        {
            if (listeners == null || !listeners.GetInvocationList().Contains(callback))
            {
                listeners += callback;
            }
            return listeners;
        }
    }

    public class Signal<T, U, V> : BaseSignal, ISignal
    {
        private event Action<T, U, V> _listener = null;
        private event Action<T, U, V> _onceListener = null;

        public Delegate Listener
        {
            get { return _listener; }
            set { _listener = (Action<T, U, V>)value; }
        }

        public void AddListener(Action<T, U, V> callback)
        {
            _listener = AddUnique(_listener, callback);
        }

        public void AddOnce(Action<T, U, V> callback)
        {
            _onceListener = AddUnique(_onceListener, callback);
        }

        public void RemoveListener(Action<T, U, V> callback)
        {
            if (_listener != null)
            {
                _listener -= callback;
            }
        }

        public void Dispatch(T type1, U type2, V type3)
        {
            if (_listener != null)
            {
                _listener(type1, type2, type3);
            }
            if (_onceListener != null)
            {
                _onceListener(type1, type2, type3);
            }
            _onceListener = null;

            object[] outv = { type1, type2, type3 };

            base.Dispatch(outv);
        }

        public override void RemoveAllListeners()
        {
            _listener = null;
            _onceListener = null;

            base.RemoveAllListeners();
        }

        private Action<T, U, V> AddUnique(Action<T, U, V> listeners, Action<T, U, V> callback)
        {
            if (listeners == null || !listeners.GetInvocationList().Contains(callback))
            {
                listeners += callback;
            }
            return listeners;
        }
    }

    public class Signal<T, U, V, W> : BaseSignal, ISignal
    {
        private event Action<T, U, V, W> _listener = null;
        private event Action<T, U, V, W> _onceListener = null;

        public Delegate Listener
        {
            get { return _listener; }
            set { _listener = (Action<T, U, V, W>)value; }
        }

        public void AddListener(Action<T, U, V, W> callback)
        {
            _listener = AddUnique(_listener, callback);
        }

        public void AddOnce(Action<T, U, V, W> callback)
        {
            _onceListener = AddUnique(_onceListener, callback);
        }

        public void RemoveListener(Action<T, U, V, W> callback)
        {
            if (_listener != null)
            {
                _listener -= callback;
            }
        }

        public void Dispatch(T type1, U type2, V type3, W type4)
        {
            if (_listener != null)
            {
                _listener(type1, type2, type3, type4);
            }
            if (_onceListener != null)
            {
                _onceListener(type1, type2, type3, type4);
            }
            _onceListener = null;

            object[] outv = { type1, type2, type3, type4 };

            base.Dispatch(outv);
        }

        public override void RemoveAllListeners()
        {
            _listener = null;
            _onceListener = null;

            base.RemoveAllListeners();
        }

        private Action<T, U, V, W> AddUnique(Action<T, U, V, W> listeners, Action<T, U, V, W> callback)
        {
            if (listeners == null || !listeners.GetInvocationList().Contains(callback))
            {
                listeners += callback;
            }
            return listeners;
        }
    }

}
