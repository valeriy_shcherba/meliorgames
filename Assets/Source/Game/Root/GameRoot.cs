﻿using Common.Singleton;
using Game.Context;
using Game.Proxy;
using UnityEngine;

namespace Game.Root
{
    public class GameRoot : UnitySingleton<GameRoot>
    {
        [SerializeField] private Transform _fieldTransform;
        
        public Transform FieldTransform => _fieldTransform;
        public GameProxy Proxy => _context.Proxy;
        
        private GameContext _context;

        private bool _isInitialized;
        
        private void Awake()
        {
            _context = new GameContext();

            Proxy.InitializeGame();
            Proxy.StartGame();
            
            _isInitialized = true;
        }

        private void Update()
        {
            if (!_isInitialized) return;
            
            _context.ManagerScope.HeartbeatManager.Update();
        }

        private void OnDestroy()
        {
            _isInitialized = false;
            Proxy.ReleaseGame();
        }
    }
}