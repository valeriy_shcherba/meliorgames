﻿using System;
using System.Collections.Generic;
using Game.Data.Types;

namespace Game.Domain
{
	[Serializable]
	public class GameSettings
	{
		public List<CreepData> Enemies;
		public List<ArcherData> Archers;
	}
}