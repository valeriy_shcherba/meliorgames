﻿using System;
using System.Collections.Generic;

namespace Game.Domain
{
	[Serializable]
	public class WaySettings
	{
		public List<TargetSettings> Targets;
	}
}