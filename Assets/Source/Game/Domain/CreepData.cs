﻿using System;
using Game.Data.Types;

namespace Game.Domain
{
    [Serializable]
    public class CreepData
    {
        public int HP;
        public int Power;
        public float Speed;
        public float AttackSpeed;
        public EnemyType Type;
    }
}

