﻿using System;
using Game.Data.Types;

namespace Game.Domain
{
	[Serializable]
	public class ArcherData
	{
		public int Power;
		public ArcherType Type;
		public float AttackSpeed;
	}
}