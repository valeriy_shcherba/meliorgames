﻿using System;
using Game.Data.Types;
using UnityEngine;

namespace Game.Domain
{
    [Serializable]
    public class TargetSettings
    {
        public EnemyType Type;
        public Vector3 StartPosition;
        public Vector3 EndPosition;
    }
}