﻿using System;
using System.Collections.Generic;

namespace Game.Domain
{
    [Serializable]
    public class LevelSettings
    {
        public List<WaySettings> Ways;
    }
}

