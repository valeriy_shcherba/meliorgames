﻿namespace Game.Data.Types
{
	public enum EnemyType
	{
		NONE,
		BAT_SWORD,
		BOMBER,
		ONAGER
	}
}