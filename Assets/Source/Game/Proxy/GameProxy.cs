﻿using Game.Context;
using Game.Data.Types;
using Game.Scopes;

namespace Game.Proxy
{
    public class GameProxy : ContextInject
    {
        public GameSignalScope SignalScope => _context.SignalScope;

        public GameProxy(GameContext context) : base(context)
        {
            
        }

        public void InitializeGame()
        {
            _initializeGameCommand.Execute();   
        }

        public void StartGame()
        {
            _startGameCommand.Execute();
        }
        
        public void ReleaseGame()
        {
            _releaseGameCommand.Execute();
        }

        public void RestartGame()
        {
             _restartGameCommand.Execute();   
        }
        
        public void StartFight()
        {
            _startFightCommand.Execute();
        }

        public void AddArcher(ArcherType type)
        {
            _addArcherCommand.Execute(type);
        }
    }
}