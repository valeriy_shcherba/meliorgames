﻿using Game.View;

namespace Game.Models
{
    public class GameModel
    {
        public FortressBuildView FortressView { get; set; }

        public int FortressHP { get; set; }

        public GameModel()
        {
            SetDefault();
        }

        public void Reset()
        {
            SetDefault();
        }

        private void SetDefault()
        {
            FortressHP = 100;
            FortressView?.UpdateView(FortressHP);
        }
    }
}