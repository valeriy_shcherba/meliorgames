﻿using System.Collections.Generic;
using System.Linq;
using Game.Managers.Pool;

namespace Game.Models
{
	public class PoolRegistryModel
	{
		public Dictionary<Poolable, int> RegisteredCreeps { get; }
		public List<Poolable> RegisteredArchers { get; }

		public PoolRegistryModel()
		{
			RegisteredCreeps = new Dictionary<Poolable, int>();
			RegisteredArchers = new List<Poolable>();
		}

		public void RegisterCreep(Poolable entity, int hp)
		{
			RegisteredCreeps.Add(entity, hp);
		}

		public void UnregisterCreep(Poolable entity)
		{
			RegisteredCreeps.Remove(entity);
		}
		
		public void RegisterArcher(Poolable entity)
		{
			RegisteredArchers.Add(entity);
		}

		public void UnregisterArcher(Poolable entity)
		{
			RegisteredArchers.Remove(entity);
		}

		public void Reset()
		{
			Poolable[] creeps = RegisteredCreeps.Keys.ToArray();
			Poolable[] archers = RegisteredArchers.ToArray();

			DespawnPoolables(creeps);
			DespawnPoolables(archers);
			
			RegisteredCreeps.Clear();
			RegisteredArchers.Clear();
		}

		private void DespawnPoolables(Poolable[] poolables)
		{
			for (int index = 0; index < poolables.Length; index++)
			{
				poolables[index].OnDespawn();
			}
		}
	}
}