﻿using System.Collections.Generic;
using Game.Api;

namespace Game.Models
{
	public class HeartbeatEntityModel
	{
		private List<IUpdatableEntity> _registeredEntities;
		
		public HeartbeatEntityModel()
		{
			_registeredEntities = new List<IUpdatableEntity>();
		}

		public void RegisterEntity(IUpdatableEntity entity)
		{
			_registeredEntities.Add(entity);
		}

		public void UnregisterEntity(IUpdatableEntity entity)
		{
			_registeredEntities.Remove(entity);
		}

		public List<IUpdatableEntity> GetAllRegisteredEntities()
		{
			return _registeredEntities;
		}
	}
}