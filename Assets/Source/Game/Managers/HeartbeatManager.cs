﻿using Game.Context;
using UnityEngine;

namespace Game.Managers
{
	public class HeartbeatManager : ContextInject
	{
		public float Time;

		private bool _isStarted;
		
		public HeartbeatManager(GameContext context) : base(context) {}

		public void Update()
		{
			if (!_isStarted) return;

			Time = UnityEngine.Time.time;
			
			var entitiesList = _heartbeatEntityModel.GetAllRegisteredEntities();

			entitiesList.ForEach(x => x.UpdateEntity());
		}

		public void SetStart(bool isStarted)
		{
			_isStarted = isStarted;
		}
	}
}