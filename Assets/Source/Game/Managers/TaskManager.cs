﻿using Game.Api;
using Game.Context;
using Game.Controllers.Tasks;

namespace Game.Managers
{
	public class TaskManager : ContextInject, IUpdatableEntity
	{
		public TaskManager(GameContext context) : base(context)
		{
			_heartbeatEntityModel.RegisterEntity(this);
		}

		public void AddTask(TaskBase task)
		{
			_taskModel.RegisterTask(task);
		}
		
		public void RemoveTask(TaskBase task)
		{
			_taskModel.UnregisterTask(task);
		}

		public void UpdateEntity()
		{
			var entities = _taskModel.GetAllRegistered();

			for (int index = 0; index < entities.Length; index++)
			{
				entities[index].UpdateEntity();
			}
		}
	}
}