﻿using System;
using UnityEngine;

namespace Game.Managers.Pool
{
	public class Poolable : MonoBehaviour
	{
		public Pool CurrentPool;

		public event Action OnDespawned; 
		
		public virtual void OnSpawn()
		{
			gameObject.SetActive(true);
		}

		public virtual void OnDespawn()
		{
			gameObject.SetActive(false);
			CurrentPool.Despawn(this);
			
			OnDespawned?.Invoke();
		}
	}
}