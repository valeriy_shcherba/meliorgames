﻿using System;
using System.Collections.Generic;
using Game.Context;
using Game.View.Consumes;
using Game.View.Enemies;
using Game.View.Units;
using UnityEngine;

namespace Game.Managers.Pool
{
	public class PoolManager : ContextInject
	{
		private Dictionary<Type, Pool> _poolsMap;
		
		public PoolManager(GameContext context) : base(context)
		{
			Bind();
			
			Precached();
		}

		private void Bind()
		{
			Transform poolsRoot = new GameObject("[POOLS]").transform;
			_poolsMap = new Dictionary<Type, Pool>();

			_poolsMap.Add(typeof(BatSwordActor), new Pool(poolsRoot,"Game/View/Enemies/Enemy - BatSword"));
			_poolsMap.Add(typeof(BomberView), new Pool(poolsRoot,"Game/View/Enemies/Enemy - Bomber"));
			_poolsMap.Add(typeof(OnagerActor), new Pool(poolsRoot,"Game/View/Enemies/Enemy - Onager"));
			
			_poolsMap.Add(typeof(Archer01Actor), new Pool(poolsRoot,"Game/View/Units/Unit - Archer01"));
			_poolsMap.Add(typeof(Archer02Actor), new Pool(poolsRoot,"Game/View/Units/Unit - Archer02"));
			_poolsMap.Add(typeof(Archer03Actor), new Pool(poolsRoot,"Game/View/Units/Unit - Archer03"));
			
			_poolsMap.Add(typeof(ArrowActor), new Pool(poolsRoot,"Game/View/Consumes/Arrow"));
			_poolsMap.Add(typeof(BombActor), new Pool(poolsRoot,"Game/View/Consumes/Bomb"));
			_poolsMap.Add(typeof(BloodActor), new Pool(poolsRoot,"Game/View/Consumes/Blood"));
		}

		private void Precached()
		{
			foreach (var pool in _poolsMap)
			{
				pool.Value.Spawn(true);
			}
		}
		
		public Poolable Spawn(Type type)
		{
			var entity = _poolsMap[type].Spawn();
			return entity;
		}
	}
}