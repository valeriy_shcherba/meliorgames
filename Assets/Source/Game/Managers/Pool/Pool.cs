﻿using System.Collections.Generic;
using System.Linq;
using Game.Root;
using UnityEditor;
using UnityEngine;

namespace Game.Managers.Pool
{
	public class Pool
	{
		private Poolable _poolableObject;
		private Transform _poolTransform;

		private Stack<Poolable> _cachedObjects;
		
		public Pool(Transform root, string resourcesPath)
		{
			_poolableObject = Resources.Load<Poolable>(resourcesPath);
			
			_poolTransform = new GameObject($"[POOL - {_poolableObject.name}]").transform;
			_poolTransform.SetParent(root);
			
			_cachedObjects = new Stack<Poolable>();
		}
		
		public Poolable Spawn(bool isPrecache = false)
		{
			Poolable obj = _cachedObjects.Any() ? _cachedObjects.Pop()
				: MonoBehaviour.Instantiate(_poolableObject, _poolTransform, false);
			
			obj.CurrentPool = this;
			obj.transform.position = Vector3.zero;
			obj.transform.eulerAngles = Vector3.zero;
			obj.transform.SetParent(GameRoot.Instance.FieldTransform, false);
			
			if (isPrecache)
			{
				obj.OnDespawn();
				return obj;
			}
			
			obj.OnSpawn();
			return obj;
		}

		public void Despawn(Poolable obj)
		{
			obj.transform.SetParent(_poolTransform);
			
			_cachedObjects.Push(obj);
		}
	}
}