﻿using UnityEngine;

namespace Game.View
{
	public class ArcherActor : GameView
	{
		[SerializeField] private Transform _spawnPoint;
		
		public Transform SpawnPoint => _spawnPoint;
	}
}