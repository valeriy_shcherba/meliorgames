﻿using System;
using Game.Managers.Pool;
using UnityEngine;

namespace Game.View
{
	public class GameView : Poolable
	{
		[SerializeField] protected Animator _animator;
		public Animator Animator => _animator;
		
		public event Action OnDamageMade;

		private void Animator_OnDamageMade()
		{
			OnDamageMade?.Invoke();
		}
	}
}