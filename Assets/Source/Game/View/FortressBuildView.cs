﻿using UnityEngine;

namespace Game.View
{
	public class FortressBuildView : GameView
	{
		[SerializeField] private SpriteRenderer _fortressRenderer;
		[SerializeField] private Sprite[] _sprites;
		[SerializeField] private Transform[] _spawnPoints;

		public Transform[] SpawnPoints => _spawnPoints;
		
		public void UpdateView(int hp)
		{
			Sprite viewSprite = null;
			
			if (hp <= 25)
			{
				viewSprite = _sprites[3];
			}
			else if (hp <= 50)
			{
				viewSprite = _sprites[2];
			}
			else if (hp <= 50)
			{
				viewSprite = _sprites[1];
			}
			else
			{
				viewSprite = _sprites[0];
			}
			
			_fortressRenderer.sprite = viewSprite;
		}
	}
}