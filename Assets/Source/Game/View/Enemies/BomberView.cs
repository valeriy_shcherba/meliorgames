﻿using UnityEngine;

namespace Game.View.Enemies
{
	public class BomberView : EnemyView
	{
		[SerializeField] private Transform _spawnPoint;

		public Transform SpawnPoint => _spawnPoint;
	}	
}