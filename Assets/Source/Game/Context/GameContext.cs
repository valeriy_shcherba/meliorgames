﻿using Game.Core;
using Game.Proxy;
using Game.Scopes;

namespace Game.Context
{
	public class GameContext
	{
		public GameProxy Proxy { get; }
		
		public GameCommandScope CommandScope { get; }
		public GameManagerScope ManagerScope { get; }
		public GameModelScope ModelScope { get; }
		public GameSignalScope SignalScope { get; }
		
		public CoreConfigurator Configurator { get; }
		
		public GameContext()
		{
			Proxy = new GameProxy(this);
			
			ModelScope = new GameModelScope();
			SignalScope = new GameSignalScope();
			CommandScope = new GameCommandScope(this);	
			ManagerScope = new GameManagerScope(this);
			
			Configurator = new CoreConfigurator();
		}
	}
}