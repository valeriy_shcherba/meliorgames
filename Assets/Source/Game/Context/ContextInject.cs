﻿using Common.Signal.Impl;
using Game.Controllers.Commands;
using Game.Controllers.Tasks;
using Game.Core;
using Game.Managers;
using Game.Managers.Pool;
using Game.Models;

namespace Game.Context
{
	public class ContextInject
	{
		protected GameContext _context;

		public ContextInject(GameContext context)
		{
			_context = context;
		}
		
		//Commands
		protected InitializeGameCommand _initializeGameCommand => _context.CommandScope.InitializeCommand;
		protected ReleaseGameCommand _releaseGameCommand => _context.CommandScope.ReleaseCommand;
		protected RestartGameCommand _restartGameCommand => _context.CommandScope.RestartGameCommand;
		protected StartGameCommand _startGameCommand => _context.CommandScope.StartCommand;
		protected StopGameCommand _stopGameCommand => _context.CommandScope.StopCommand;

		protected PrepareFieldCommand _prepareFieldCommand => _context.CommandScope.PrepareFieldCommand;
		protected AttackFortressCommand _attackFortressCommand => _context.CommandScope.AttackFortressCommand;
		
		protected AddArcherCommand _addArcherCommand => _context.CommandScope.AddArcherCommand;
		protected StartFightCommand _startFightCommand => _context.CommandScope.StartFightCommand;
		
		//Managers
		protected PoolManager _poolManager => _context.ManagerScope.PoolManager;
		protected HeartbeatManager _heartbeatManager => _context.ManagerScope.HeartbeatManager;
		protected TaskManager _taskManager => _context.ManagerScope.TaskManager;
		
		//Models
		protected HeartbeatEntityModel _heartbeatEntityModel => _context.ModelScope.HeartbeatEntityModel;
		protected GameModel _gameModel => _context.ModelScope.GameModel;
		protected TaskRegistryModel _taskModel => _context.ModelScope.TaskModel;
		protected PoolRegistryModel _poolModel => _context.ModelScope.PoolModel;
		
		//Signals
		protected Signal _gameInitialized => _context.SignalScope.GameInitializedSignal;
		protected Signal _gameSuccessfullyFinishedSignal => _context.SignalScope.GameSuccessfullyFinishedSignal;
		protected Signal _gameUnsuccessfullyFinishedSignal => _context.SignalScope.GameUnsuccessfullyFinishedSignal;
		
		//Configurator
		protected CoreConfigurator _configurator => _context.Configurator;
	}
}