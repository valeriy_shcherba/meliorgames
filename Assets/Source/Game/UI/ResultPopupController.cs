﻿using App.Const;
using Game.Proxy;
using Game.Root;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game.UI
{
    [RequireComponent(typeof(ResultPopupView))]
    public class ResultPopupController : MonoBehaviour
    {
        [SerializeField] private ResultPopupView _view;

        private GameProxy _proxy;

        public void Setup(string title)
        {
            _view.TitleText.text = title;
        }

        public void Show()
        {
            gameObject.SetActive(true);
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
        
        private void OnEnable()
        {
            _view.ReplayButton.onClick.AddListener(OnButtonReplayClicked);
            _view.MenuButton.onClick.AddListener(OnButtonMenuClicked);
        }

        private void OnDisable()
        {
            _view.ReplayButton.onClick.RemoveAllListeners();
            _view.MenuButton.onClick.RemoveAllListeners();
        }

        private void Start()
        {
            _proxy = GameRoot.Instance.Proxy;
        }

        private void OnButtonReplayClicked()
        {
            _proxy.RestartGame();
            
            Hide();
        }

        private void OnButtonMenuClicked()
        {
            _proxy.ReleaseGame();
            
            Hide();
            
            SceneManager.LoadScene(AppConstants.MAINMENU_SCENE_NAME);
        }
    }
}

