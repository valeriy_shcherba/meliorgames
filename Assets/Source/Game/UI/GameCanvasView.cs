﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
	public class GameCanvasView : MonoBehaviour
	{
		[SerializeField] private ResultPopupController _resultPopup;
		
		[SerializeField] private Button _startFightButton;
		[SerializeField] private Button _addArcher01Button;
		[SerializeField] private Button _addArcher02Button;
		[SerializeField] private Button _addArcher03Button;
		
		public ResultPopupController ResultPopup => _resultPopup;
		
		public Button StartFightButton => _startFightButton;
		public Button AddArcher01Button => _addArcher01Button;
		public Button AddArcher02Button => _addArcher02Button;
		public Button AddArcher03Button => _addArcher03Button;
	}
}


