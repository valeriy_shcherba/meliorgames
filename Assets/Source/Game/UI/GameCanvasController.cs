﻿using Game.Data.Types;
using Game.Proxy;
using Game.Root;
using UnityEngine;

namespace Game.UI
{
	[RequireComponent(typeof(GameCanvasView))]
	public class GameCanvasController : MonoBehaviour
	{
		[SerializeField] private GameCanvasView _view;

		private GameProxy _proxy;

		private void OnEnable()
		{
			_view.StartFightButton.onClick.AddListener(OnButtonStartFightClicked);
			_view.AddArcher01Button.onClick.AddListener(OnButtonAddArcher01Clicked);
			_view.AddArcher02Button.onClick.AddListener(OnButtonAddArcher02Clicked);
			_view.AddArcher03Button.onClick.AddListener(OnButtonAddArcher03Clicked);
		}

		private void OnDisable()
		{
			_view.StartFightButton.onClick.RemoveAllListeners();
			_view.AddArcher01Button.onClick.RemoveAllListeners();
			_view.AddArcher02Button.onClick.RemoveAllListeners();
			_view.AddArcher03Button.onClick.RemoveAllListeners();
		}

		private void Start()
		{
			_proxy = GameRoot.Instance.Proxy;
			
			_proxy.SignalScope.GameSuccessfullyFinishedSignal.AddListener(GameProxy_GameSuccessfullyFinished);
			_proxy.SignalScope.GameUnsuccessfullyFinishedSignal.AddListener(GameProxy_GameUnsuccessfullyFinished);
		}

		private void OnDestroy()
		{
			_proxy.SignalScope.GameSuccessfullyFinishedSignal.RemoveAllListeners();
			_proxy.SignalScope.GameUnsuccessfullyFinishedSignal.RemoveAllListeners();
		}

		private void OnButtonStartFightClicked()
		{
			_proxy.StartFight();
		}

		private void OnButtonAddArcher01Clicked()
		{
			_proxy.AddArcher(ArcherType.ARCHER_01);
		}

		private void OnButtonAddArcher02Clicked()
		{
			_proxy.AddArcher(ArcherType.ARCHER_02);
		}
		
		private void OnButtonAddArcher03Clicked()
		{
			_proxy.AddArcher(ArcherType.ARCHER_03);
		}

		private void GameProxy_GameSuccessfullyFinished()
		{
			_view.ResultPopup.Setup("WIN");
			_view.ResultPopup.Show();
		}
		
		private void GameProxy_GameUnsuccessfullyFinished()
		{
			_view.ResultPopup.Setup("LOSE");
			_view.ResultPopup.Show();
		}
	}
}