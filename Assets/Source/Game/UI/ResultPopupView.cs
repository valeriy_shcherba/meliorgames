﻿using UnityEngine;
using UnityEngine.UI;

namespace Game.UI
{
	public class ResultPopupView : MonoBehaviour
	{
		[SerializeField] private Button _replayButton;
		[SerializeField] private Button _menuButton;
		
		[SerializeField] private Text _titleText;

		public Button ReplayButton => _replayButton;
		public Button MenuButton => _menuButton;
		
		public Text TitleText => _titleText;
	}
}