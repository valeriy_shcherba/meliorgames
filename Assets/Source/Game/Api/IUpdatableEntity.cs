﻿namespace Game.Api
{
    public interface IUpdatableEntity
    {
        void UpdateEntity();
    }
}

