﻿using Game.Context;
using Game.Root;
using Game.View;
using UnityEngine;

namespace Game.Controllers.Commands
{
	public class PrepareFieldCommand : Command
	{
		public PrepareFieldCommand(GameContext context) : base(context)
		{
			
		}

		public override void Execute(params object[] objs)
		{
			FortressBuildView buildView = Resources.Load<FortressBuildView>("Game/View/Fortres's/Build - Fortress04");
			buildView = MonoBehaviour.Instantiate(buildView);
			buildView.transform.SetParent(GameRoot.Instance.FieldTransform, false);
			
			_gameModel.FortressView = buildView;
		}
	}
}