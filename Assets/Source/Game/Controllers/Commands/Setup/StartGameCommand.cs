﻿using DG.Tweening;
using Game.Context;

namespace Game.Controllers.Commands
{
	public class StartGameCommand : Command
	{
		public StartGameCommand(GameContext context) : base(context)
		{
			
		}

		public override void Execute(params object[] objs)
		{
			_heartbeatManager.SetStart(true);
			DOTween.timeScale = 1f;
		}
	}
}