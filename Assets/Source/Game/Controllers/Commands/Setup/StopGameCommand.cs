﻿using DG.Tweening;
using Game.Context;

namespace Game.Controllers.Commands
{
	public class StopGameCommand : Command
	{
		public StopGameCommand(GameContext context) : base(context)
		{	
		}

		public override void Execute(params object[] objs)
		{
			_heartbeatManager.SetStart(false);
			DOTween.timeScale = 0f;
		}
	}
}