﻿using Game.Context;

namespace Game.Controllers.Commands
{
	public class RestartGameCommand : Command
	{
		public RestartGameCommand(GameContext context) : base(context)
		{
		}

		public override void Execute(params object[] objs)
		{
			_taskModel.Reset();
			_gameModel.Reset();
			_poolModel.Reset();
			
			_configurator.Reset();
			
			_startGameCommand.Execute();
		}
	}
}