﻿using Game.Context;
using UnityEngine;

namespace Game.Controllers.Commands
{
    public class InitializeGameCommand : Command
    {
        public InitializeGameCommand(GameContext context) : base(context)
        {
        }

        public override void Execute(params object[] objs)
        {
            _prepareFieldCommand.Execute();
            
            _gameInitialized.Dispatch();
        }
    }
}