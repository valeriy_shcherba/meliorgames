﻿using Game.Context;

namespace Game.Controllers.Commands
{
	public class ReleaseGameCommand : Command
	{
		public ReleaseGameCommand(GameContext context) : base(context)
		{
			_context = null;
		}
	}
}