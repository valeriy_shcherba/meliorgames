﻿using Game.Context;

namespace Game.Controllers.Commands
{
	public class Command : ContextInject
	{
		public Command(GameContext context) : base(context) {}

		public virtual void Execute(params object[] objs)
		{
			
		}
	}
}