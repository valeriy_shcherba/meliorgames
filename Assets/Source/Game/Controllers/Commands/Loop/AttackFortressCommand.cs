﻿using Game.Context;
using Game.UI;
using Game.View;
using UnityEngine;

namespace Game.Controllers.Commands
{
	public class AttackFortressCommand : Command
	{
		public AttackFortressCommand(GameContext context) : base(context)
		{
		}

		public override void Execute(params object[] objs)
		{
			int damage = (int) objs[0];

			_gameModel.FortressHP -= damage;

			FortressBuildView view = _gameModel.FortressView;
			view.UpdateView(_gameModel.FortressHP);

			if (_gameModel.FortressHP <= 0)
			{
				_stopGameCommand.Execute();
				
				_gameUnsuccessfullyFinishedSignal.Dispatch();
			}
		}
	}
}
