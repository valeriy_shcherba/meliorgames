﻿using System;
using Game.Context;
using Game.Controllers.Tasks;
using Game.Data.Types;
using Game.Managers.Pool;
using Game.View;
using Game.View.Units;
using UnityEngine;

namespace Game.Controllers.Commands
{
	public class AddArcherCommand : Command
	{
		public AddArcherCommand(GameContext context) : base(context)
		{
		}

		public override void Execute(params object[] objs)
		{
			ArcherType type = (ArcherType)objs[0];

			FortressBuildView fortress = _gameModel.FortressView;

			int amountArchers = _poolModel.RegisteredArchers.Count;
			if (amountArchers >= fortress.SpawnPoints.Length) return;

			Transform spawnPoint = fortress.SpawnPoints[amountArchers];
			
			Type archerType = null;

			if (type == ArcherType.ARCHER_01)
			{
				archerType = typeof(Archer01Actor);
			}
			else if (type == ArcherType.ARCHER_02)
			{
				archerType = typeof(Archer02Actor);
			}
			else if (type == ArcherType.ARCHER_03)
			{
				archerType = typeof(Archer03Actor);
			}
			
			Poolable archer = _poolManager.Spawn(archerType);
			archer.transform.SetParent(spawnPoint, false);
			
			_poolModel.RegisterArcher(archer);

			ControllArcherTask task = new ControllArcherTask(_context);
			task.Setup(archer.GetComponent<ArcherActor>(), _configurator.DictionaryArchers[type]);
			task.Start();
		}
	}
}