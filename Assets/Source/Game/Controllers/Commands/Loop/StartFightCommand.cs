﻿using Game.Context;
using Game.Controllers.Tasks;
using Game.Core;
using Game.Domain;

namespace Game.Controllers.Commands
{
	public class StartFightCommand : Command
	{
		public StartFightCommand(GameContext context) : base(context)
		{
		
		}

		public override void Execute(params object[] objs)
		{
			CoreLogic logic = _configurator.CoreLogic;

			WaySettings settings = logic.GetNextWaySettings();
			
			if (settings == null) return;
			
			GenerateWayTask task = new GenerateWayTask(_context);
			task.Setup(settings.Targets);
			task.Start();
		}
	}
}