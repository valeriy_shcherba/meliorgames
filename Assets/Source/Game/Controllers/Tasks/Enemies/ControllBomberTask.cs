﻿using DG.Tweening;
using Game.Context;
using Game.Domain;
using Game.Managers.Pool;
using Game.Root;
using Game.View;
using Game.View.Consumes;
using Game.View.Enemies;
using UnityEngine;

namespace Game.Controllers.Tasks
{
	public class ControllBomberTask : ControllEnemyTask
	{
		public ControllBomberTask(GameContext gameContext) : base(gameContext)
		{
		}

		private BomberView _view;

		private Poolable _projectTile;
		
		public override void Setup(EnemyView view, CreepData data, Vector3 endPosition)
		{
			base.Setup(view, data, endPosition);
			
			_view = view as BomberView;
		}

		protected override void View_OnDamageMake()
		{
			_projectTile = _poolManager.Spawn(typeof(BombActor));
			_projectTile.transform.position = _view.SpawnPoint.position;

			Vector3 endPosition = _view.SpawnPoint.position;
			endPosition.x += 1;
			
			_projectTile.transform.DOLocalJump(endPosition, .5f, 1, .5f).OnComplete(OnProjectTileMoveCompleted).Play();
		}

		private void OnProjectTileMoveCompleted()
		{
			ExecuteAttackCommand();
			
			_projectTile.OnDespawn();
			_projectTile = null;
		}
	}
}
