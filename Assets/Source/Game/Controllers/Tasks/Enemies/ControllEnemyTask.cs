﻿using DG.Tweening;
using Game.Context;
using Game.Domain;
using Game.View;
using UnityEngine;

namespace Game.Controllers.Tasks
{
	public class ControllEnemyTask : TaskGame
	{
		public ControllEnemyTask(GameContext gameContext) : base(gameContext)
		{
		}

		private const string TRIGGER_ATTACK = "ATTACK";
		
		private EnemyView _view;
		private CreepData _data;

		private Tween _tweenMovement;

		private bool _canAttack;
		private float _attackDelay;
		
		public virtual void Setup(EnemyView view, CreepData data, Vector3 endPosition)
		{
			_view = view;
			_data = data;

			_view.OnDamageMade += View_OnDamageMake;
			_view.OnDespawned += View_OnDespawned;
			
			_tweenMovement = _view.transform.DOMove(endPosition, data.Speed).OnComplete(OnMoveCompleted);
			
			_poolModel.RegisterCreep(view, _data.HP);
		}

		protected override void Release()
		{
			_view.OnDamageMade -= View_OnDamageMake;
			_view.OnDespawned -= View_OnDespawned;
		}

		protected override void TickUpdate()
		{
			base.TickUpdate();
			
			if (!_canAttack) return;
			if (_heartbeatManager.Time <= _attackDelay) return;

			_attackDelay = _heartbeatManager.Time + _data.AttackSpeed;
			_view.Animator.SetTrigger(TRIGGER_ATTACK);
		}

		public override void Start()
		{
			base.Start();
			
			_tweenMovement.Play();
		}

		private void OnMoveCompleted()
		{
			_canAttack = true;
		}

		private void View_OnDespawned()
		{
			Stop(true);
		}
		
		protected void ExecuteAttackCommand()
		{
			_attackFortressCommand.Execute(_data.Power);
		}
		
		protected virtual void View_OnDamageMake()
		{
			ExecuteAttackCommand();
		}
	}
}