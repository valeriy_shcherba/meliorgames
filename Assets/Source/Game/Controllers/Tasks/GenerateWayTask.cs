﻿using System;
using System.Collections.Generic;
using System.Linq;
using Game.Context;
using Game.Data.Types;
using Game.Domain;
using Game.Root;
using Game.View;
using Game.View.Enemies;
using UnityEngine;

namespace Game.Controllers.Tasks
{
	public class GenerateWayTask : TaskGame
	{
		public GenerateWayTask(GameContext gameContext) : base(gameContext)
		{
		}

		private List<TargetSettings> _targets;

		private const float DELAY_OF_CREEPS = 1.5f;
		
		private int _currentCreepNumber = 0;
		private int _maxCreepNumber = 0;
		
		private float _timeLastCreateCreep;
		
		public void Setup(List<TargetSettings> targets)
		{
			_targets = targets;

			_maxCreepNumber = _targets.Count();
		}

		protected override void TickUpdate ()
		{
			base.TickUpdate ();

			if (_currentCreepNumber == _maxCreepNumber)
			{
				Stop(true);

				return;
			}

			if (!(_heartbeatManager.Time > _timeLastCreateCreep + DELAY_OF_CREEPS)) return;
			
			_timeLastCreateCreep = _heartbeatManager.Time;
			CreateGameActor(_targets[_currentCreepNumber]);
			_currentCreepNumber++;
		}

		private void CreateGameActor(TargetSettings settings)
		{
			Type creepType = null;

			if (settings.Type == EnemyType.BAT_SWORD)
			{
				creepType = typeof(BatSwordActor);
			}
			else if (settings.Type == EnemyType.BOMBER)
			{
				creepType = typeof(BomberView);
			}
			else if (settings.Type == EnemyType.ONAGER)
			{
				creepType = typeof(OnagerActor);
			}
			
			Transform obj = _poolManager.Spawn(creepType).transform;
			obj.SetParent(GameRoot.Instance.FieldTransform);
			obj.position = settings.StartPosition;

			EnemyView view = obj.GetComponent<EnemyView>();
			
			ControllEnemyTask task = null;

			if (settings.Type == EnemyType.BOMBER)
			{
				task = new ControllBomberTask(_context);
			}
			else task = new ControllEnemyTask(_context);

			task.Setup(view, _configurator.DictionaryEnemies[settings.Type], settings.EndPosition);
			task.Start();
		}
	}
}