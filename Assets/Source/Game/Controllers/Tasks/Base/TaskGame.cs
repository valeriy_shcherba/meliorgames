﻿using Game.Context;
using Game.Data.Types;

namespace Game.Controllers.Tasks
{
	public class TaskGame: TaskBase
	{
		public TaskGame(GameContext gameContext): base(gameContext){}

		protected override void TickUpdate ()
		{
			if (_state == TaskState.STARTED)
			{
				float currentTime = _heartbeatManager.Time;
				float startTimeWithDelay = _startTime + _delay + (_stopPauseDelay - _startPauseDelay);

				if (currentTime < startTimeWithDelay) {
					return;
				}

				_startTime = currentTime;
	
				ChangeState (TaskState.RUNNING);
			} 

		}

		public virtual void Execute() {}
	}
}