﻿using System.Linq;
using Common.Utils;
using DG.Tweening;
using Game.Context;
using Game.Domain;
using Game.Managers.Pool;
using Game.View;
using Game.View.Consumes;
using UnityEngine;

namespace Game.Controllers.Tasks
{
	public class ControllArcherTask : TaskGame
	{
		public ControllArcherTask(GameContext gameContext) : base(gameContext)
		{
		}

		private const string TRIGGER_ATTACK = "SHOOT";
		private const string TRIGGER_ENEMY_DEATH = "DEATH";
		
		private ArcherActor _view;
		private ArcherData _data;

		private Poolable _projectTile;
		private Poolable _entity;

		private float _attackDelay;
		
		public void Setup(ArcherActor view, ArcherData data)
		{
			_view = view;
			_data = data;

			_view.OnDamageMade += ArcherView_OnDamageMade;
		}

		protected override void Release()
		{
			_view.OnDamageMade -= ArcherView_OnDamageMade;
		}
		
		protected override void TickUpdate()
		{
			base.TickUpdate();

			if (_poolModel.RegisteredCreeps.Count == 0) return;
			if (_heartbeatManager.Time <= _attackDelay) return;
			
			_entity = _entity ?? _poolModel.RegisteredCreeps.Keys.First();
			
			_attackDelay = _heartbeatManager.Time + _data.AttackSpeed;
			_view.Animator.SetTrigger(TRIGGER_ATTACK);
		}

		private void ArcherView_OnDamageMade()
		{
			if (_entity == null) return;
			
			_projectTile = _projectTile ?? _poolManager.Spawn(typeof(ArrowActor));
			_projectTile.transform.position = _view.SpawnPoint.position;
			_projectTile.transform.rotation = VectorUtils.RotateToObject(_projectTile.transform, _entity.transform);

			Vector3 endPosition = _entity.transform.position;
			
			_projectTile.transform.DOMove(endPosition, .25f).OnComplete(OnProjectTileMoveCompleted).Play();
		}

		private void OnProjectTileMoveCompleted()
		{
			_projectTile?.OnDespawn();
			_projectTile = null;

			if (!_poolModel.RegisteredCreeps.ContainsKey(_entity))
			{
				_entity = null;
				return;
			}

			var blood = _poolManager.Spawn(typeof(BloodActor));
			blood.transform.position = _entity.transform.position;
			
			_poolModel.RegisteredCreeps[_entity] -= _data.Power;

			if (_poolModel.RegisteredCreeps[_entity] > 0) return;
			
			_poolModel.UnregisterCreep(_entity);

			var actor = _entity.GetComponent<EnemyView>();
			actor.Animator.SetTrigger(TRIGGER_ENEMY_DEATH);
			
			if (_configurator.CoreLogic.IsLevelCompleted() && !_poolModel.RegisteredCreeps.Any())
			{
				_stopGameCommand.Execute();
				_gameSuccessfullyFinishedSignal.Dispatch();
			}
		}
	}
}