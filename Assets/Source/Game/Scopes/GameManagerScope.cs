﻿using Game.Context;
using Game.Managers;
using Game.Managers.Pool;

namespace Game.Scopes
{
	public class GameManagerScope
	{
		public PoolManager PoolManager { get; }
		public HeartbeatManager HeartbeatManager { get; }
		public TaskManager TaskManager { get; }
		
		public GameManagerScope(GameContext context)
		{
			PoolManager = new PoolManager(context);
			HeartbeatManager = new HeartbeatManager(context);
			TaskManager = new TaskManager(context);
		}
	}
}