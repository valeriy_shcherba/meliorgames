﻿using Game.Models;

namespace Game.Scopes
{
	public class GameModelScope
	{
		public HeartbeatEntityModel HeartbeatEntityModel { get; }
		public GameModel GameModel { get; }
		public TaskRegistryModel TaskModel { get; }
		public PoolRegistryModel PoolModel { get; }
		
		public GameModelScope()
		{
			HeartbeatEntityModel = new HeartbeatEntityModel();
			GameModel = new GameModel();
			TaskModel = new TaskRegistryModel();
			PoolModel = new PoolRegistryModel();
		}
	}
}