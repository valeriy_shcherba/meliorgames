﻿using Game.Context;
using Game.Controllers.Commands;

namespace Game.Scopes
{
    public class GameCommandScope
    {
        //Setup
        public InitializeGameCommand InitializeCommand { get; }
        public ReleaseGameCommand ReleaseCommand { get; }
        public RestartGameCommand RestartGameCommand { get; }
        public StartGameCommand StartCommand { get; }
        public StopGameCommand StopCommand { get; }
        
        //Field
        public PrepareFieldCommand PrepareFieldCommand { get; }
        public AttackFortressCommand AttackFortressCommand { get; }
        
        //Loop
        public AddArcherCommand AddArcherCommand { get; }
        public StartFightCommand StartFightCommand { get; }
        
        public GameCommandScope(GameContext context)
        {
            InitializeCommand = new InitializeGameCommand(context);
            ReleaseCommand = new ReleaseGameCommand(context);
            StartCommand = new StartGameCommand(context);
            StopCommand = new StopGameCommand(context);
            RestartGameCommand = new RestartGameCommand(context);
            
            PrepareFieldCommand = new PrepareFieldCommand(context);
            AttackFortressCommand = new AttackFortressCommand(context);
            
            AddArcherCommand = new AddArcherCommand(context);
            StartFightCommand = new StartFightCommand(context);
        }
    }
}