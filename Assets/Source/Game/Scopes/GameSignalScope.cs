﻿using Common.Signal.Impl;
using Game.Controllers.Tasks;

namespace Game.Scopes
{
	public class GameSignalScope
	{
		public Signal GameInitializedSignal { get; }
		
		public Signal GameSuccessfullyFinishedSignal { get; }
		public Signal GameUnsuccessfullyFinishedSignal { get; }
		
		public GameSignalScope()
		{
			GameInitializedSignal = new Signal();
			GameSuccessfullyFinishedSignal = new Signal();
			GameUnsuccessfullyFinishedSignal = new Signal();
		}
	}
}