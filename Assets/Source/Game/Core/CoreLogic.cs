﻿using System.Collections.Generic;
using System.Linq;
using Game.Domain;

namespace Game.Core
{
	public class CoreLogic
	{
		private LevelSettings _levelSettings;

		private List<WaySettings> _ways;
		
		public CoreLogic(LevelSettings levelSettings)
		{
			_levelSettings = levelSettings;

			_ways = new List<WaySettings>(_levelSettings.Ways);
		}

		public WaySettings GetNextWaySettings()
		{
			if (_ways.Count == 0) return null;
			
			WaySettings settings = _ways.First();
			_ways.Remove(settings);

			return settings;
		}

		public void Reset()
		{
			_ways = new List<WaySettings>(_levelSettings.Ways);
		}

		public bool IsLevelCompleted()
		{
			return !_ways.Any();
		}
	}
}