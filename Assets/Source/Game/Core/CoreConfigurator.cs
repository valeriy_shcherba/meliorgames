﻿using System.Collections.Generic;
using System.Linq;
using Game.Data.Types;
using Game.Domain;
using UnityEngine;

namespace Game.Core
{
	public class CoreConfigurator
	{
		private const string SETTINGS_PATH = "Game/Data/data_settings";
		private const string LEVEL_SETTINGS_PATH = "Game/Data/data_level01";

		public Dictionary<EnemyType, CreepData> DictionaryEnemies { get; }
		public Dictionary<ArcherType, ArcherData> DictionaryArchers { get; }
		
		public CoreLogic CoreLogic { get; }

		public CoreConfigurator()
		{
			string gameConfig = Resources.Load<TextAsset>(SETTINGS_PATH).text;
			string levelConfig = Resources.Load<TextAsset>(LEVEL_SETTINGS_PATH).text;
			
			GameSettings gameSettings = JsonUtility.FromJson<GameSettings>(gameConfig);
			LevelSettings levelSettings = JsonUtility.FromJson<LevelSettings>(levelConfig);

			DictionaryEnemies = gameSettings.Enemies.ToDictionary(x => x.Type, x => x);
			DictionaryArchers = gameSettings.Archers.ToDictionary(x => x.Type, x => x);
			
			CoreLogic = new CoreLogic(levelSettings);
		}

		public void Reset()
		{
			CoreLogic.Reset();
		}
	}
}