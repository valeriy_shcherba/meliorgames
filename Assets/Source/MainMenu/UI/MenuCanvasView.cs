﻿using UnityEngine;
using UnityEngine.UI;

namespace MainMenu.UI
{
	public class MenuCanvasView : MonoBehaviour
	{
		[SerializeField] private Button _playGameButton;
		[SerializeField] private Button _moreGamesButton;
		[SerializeField] private Button _quitGameButton;

		public Button QuitGameButton => _quitGameButton;
		public Button MoreGamesButton => _moreGamesButton;
		public Button PlayGameButton => _playGameButton;
	}
}