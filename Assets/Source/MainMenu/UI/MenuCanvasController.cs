﻿using App.Const;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace MainMenu.UI
{
	[RequireComponent(typeof(MenuCanvasView))]
	public class MenuCanvasController : MonoBehaviour
	{
		[SerializeField] private MenuCanvasView _view;

		private void OnEnable()
		{
			_view.PlayGameButton.onClick.AddListener(OnButtonPlayGameClicked);
			_view.MoreGamesButton.onClick.AddListener(OnButtonMoreGamesClicked);
			_view.QuitGameButton.onClick.AddListener(OnButtonQuitGameClicled);
		}

		private void OnDisable()
		{
			_view.PlayGameButton.onClick.RemoveAllListeners();
			_view.MoreGamesButton.onClick.RemoveAllListeners();
			_view.QuitGameButton.onClick.RemoveAllListeners();
		}

		private void OnButtonPlayGameClicked()
		{
			SceneManager.LoadScene(AppConstants.GAME_SCENE_NAME);
		}

		private void OnButtonMoreGamesClicked()
		{
			//TODO: need to impl logic of show more games.
		}

		private void OnButtonQuitGameClicled()
		{
			Application.Quit();
		}
	}
}