﻿namespace App.Const
{
	public class AppConstants
	{
		public static string GAME_SCENE_NAME = "Game";
		public static string MAINMENU_SCENE_NAME = "MainMenu";
	}
}