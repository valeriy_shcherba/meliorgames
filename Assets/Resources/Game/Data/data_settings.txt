{
  "Enemies": [
    {
      "Power": 5,
      "HP": 20,
      "Speed": 5,
      "AttackSpeed": 3,
      "Type": 1
    },
    {
      "Power": 20,
      "HP": 30,
      "Speed": 7,
      "AttackSpeed": 2,
      "Type": 2
    },
    {
      "Power": 40,
      "HP": 40,
      "Speed": 10,
      "AttackSpeed": 5,
      "Type": 3
    }
  ],
  "Archers": [
      {
        "Power": 1,
        "AttackSpeed": 1,
        "Type": 1
      },
      {
        "Power": 3,
        "AttackSpeed": 2,
        "Type": 2
      },
      {
        "Power": 6,
        "AttackSpeed": 3,
        "Type": 3
      }
    ]
}