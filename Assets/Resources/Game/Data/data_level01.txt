{
  "Ways": [
    {
      "Targets": [
        {
          "Type": 1,
          "StartPosition": {
            "x": -6.75,
            "y": -0.15,
            "z": 0
          },
          "EndPosition": {
            "x": 2.75,
            "y": -0.15,
            "z": 0
          }
        },
        {
          "Type": 2,
          "StartPosition": {
            "x": -6.75,
            "y": -1.5,
            "z": 0
          },
          "EndPosition": {
            "x": 2.75,
            "y": -1.5,
            "z": 0
          }
        }
      ]
    },
    {
      "Targets": [
        {
          "Type": 2,
          "StartPosition": {
            "x": -6.75,
            "y": -2.6,
            "z": 0
          },
          "EndPosition": {
            "x": 3.63,
            "y": -2.7,
            "z": 0
          }
        },
        {
        "Type": 3,
                  "StartPosition": {
                    "x": -6.75,
                    "y": -2.6,
                    "z": 0
                  },
                  "EndPosition": {
                    "x": 2.3,
                    "y": -1.9,
                    "z": 0
                  }
        }
      ]
    }
  ]
}